# Documentation Wapps

- [Documentation Wapps](#documentation-wapps)
  - [1 Construire un conteneur](#1-construire-un-conteneur)
  - [2 Utiliser un conteneur](#2-utiliser-un-conteneur)
    - [2.1 Avec interface graphique](#21-avec-interface-graphique)
    - [2.2 En ligne de commande](#22-en-ligne-de-commande)
      - [2.2.1 En local](#221-en-local)
      - [2.2.2 Sur un cluster](#222-sur-un-cluster)
  - [3 Modifier un workflow](#3-modifier-un-workflow)

Le dépôt [Wapps](https://gitlab.mbb.univ-montp2.fr/mmassaviol/wapps) (Workflow applications) contient les fichiers nécessaires pour créer des conteneurs d'application (environnement isolé et portable avec ses propres dépendances logicielles) générés à l'aide du dépot [WAW](https://gitlab.mbb.univ-montp2.fr/mmassaviol/waw) (Workflow Application Wrapper).

Chaque workflow est composé de deux parties :

- Un ensemble de fichier permettant d'exécuter le workflow (cela correspond au sous-dossier files de chacun des workflows du dépot Wapps) :
  - *Snakefile* : Pour gérer l'exécution des workflow nous utilisons **Snakemake** (système de gestion de workflow). Les outils sont intégrés sous la forme de règle(s) décrivant les entrées et sorties ainsi que les paramètres et la commande à lancer pour executer l'outil. Ces règles sont regroupées dans un fichier Snakefile qui contient aussi le code nécessaire pour lier les outils de sorte à former un workflow d'analyse
  - *singularity.def* : Une recette de conteneur **Singularity** qui va permettre d'installer les outils et fichiers nécessaires à l'exécution du workflow.
  - *params.total.yml* : Un fichier de configuration au format **YAML** (extension .yml) permettant de paramétrer le workflow (choix des outils par étape, des données en entrée et des paramètres pour les différentes étapes).
  - *generate_multiqc_config.py*, *get_samples.py*, *tools.py* : Des scripts **Python** utilisés au cours du workflow
- Une application **R Shiny** permettant d'avoir une interface graphique pour paramétrer le workflow et l'exécuter (cela correspond au sous-dossier sagApp de chacun des workflows du dépot Wapps).

Liste des workflows existants:

- Metabarcoding (avec dada2)
- Mito_Assembler_Megahit (Megahit + MitoZ)
- MitoZ
- RADseq_ref (stacks)
- RADseq_denovo (stacks)
- Virus_Assembler_Megahit (Megahit + Blast + BWA)
- RNAseq (pseudoalign (kallisto, Salmon) + expression différentielle (Edger, DESeq2))
- Variant_calling (bwa or bowtie and gatk or bcftools)
- Interop_report (Illumina InterOp)
- Genome_Profile (Jellyfish + GenomeScope)

## 1 Construire un conteneur

Pour construire un conteneur on utilise **Singularity**. **Singularity** permet de construire un conteneur portable embarquant son propre système ainsi que tous les outils et fichiers que l'on veut.

Un conteneur peut-être construit directement à partir d'un conteneur existant sur le [Singularity hub](https://singularity-hub.org/) ou sur le [Docker hub](https://hub.docker.com/) ou à partir d'un conteneur déjà présent sur sa propre machine.

Un conteneur peut aussi être construit à partir d'un fichier recette. Cette recette contient plusieurs sections permettant de définir les façons d'utiliser le conteneur mais aussi et surtout une section accueillant des lignes de commande **bash** permettant d'installer des outils. Le fichier recette commence par les lignes *bootstrap* et *from* qui permmettent d'indiquer la source et le nom du conteneur de base. *bootstrap* prends les valeurs : docker pour Docker hub, shub pour Singularity hub et localimage pour un conteneur local. La rubrique from permet le conteneur qui sera la base pour le notre (dans notre cas 'rocker/r-ver:3.5.3' permet par exemple d'utiliser un conteneur docker contenant la version 3.5.3 de R). Dans le cas d'une localimage il suffit de donner l'adresse du conteneur.

Dans notre cas on crée un premier conteneur qui contient les outils basiques et communs à tous les workflows pour gagner du temps par la suite (ce sera notre base pour tous les conteneurs de workflows). Pour construire ce conteneur il faut le programme [Singularity](https://singularity.lbl.gov/) (disponible sous Linux) et le dépôt git de Wapps. Il faut aussi les droits administrateur sur votre machine pour pouvoir construire le conteneur (ils ne sont pas nécessaires pour l'utilisation du conteneur, il peut donc être construit à un endroit et être copié et utilisé ailleurs).

```bash
sudo singularity build base.sif base.def
```

Ici on se place à la racine du dépôt et on construit le conteneur de base (qui contient R, Snakemake, MultiQC, FastQC, ...). On obtient un executable du nom de *base.sif* (.sif = Singularity Image Format). On peut ensuite construire le conteneur du workflow choisi. Pour cela il suffit de se déplacer dans le dossier du workflow et de lancer la commande suivante :

```bash
sudo singularity build NomDuWorkflow.sif files/singularity.def
```

On obtient le conteneur *NomDuWorkflow.sif* prêt à être utilisé. On peut l'utiliser en local ou le copier sur n'importe quelle machine ayant une version de **Singularity** égale ou supérieure à celle qui en a permis la construction.

## 2 Utiliser un conteneur

Il existe deux façons d'utiliser le conteneur. On peut lancer l'interface shiny afin de paramétrer le workflow et de l'exécuter. On peut aussi fournir un fichier de paramètres et exécuter le workflow directement depuis la console.

Dans les deux cas il va falloir lier les dossiers nécessaires pour qu'ils soient accessibles dans le conteneur. Dans le cas des workflows WAW il faut au minimum lier 2 dossiers, un dossier Data et un dossier Results. Pour lier un dossier à un conteneur **Singularity** il faut utiliser l'option -B (ou --bind) suivie du chemin du dossier à lier séparé par ':' du chemin auquel le dossier sera accessible dans le conteneur. Par exemple pour le dossier de données (-B /dossier/de/données:/Data) qui dans le conteneur seront accessibles dans /Data et le dossier dans lequel les résultats seront écrits (-B /dossier/de/résultats:/Results) qui dans le conteneur correspond à l'adresse /Results.

### 2.1 Avec interface graphique

Pour lancer l'application sur la machine ou se trouve le conteneur il faut lancer la commande :

```bash
singularity run --app UI -B /chemin/vers/les/données:/Data -B /dossier/de/résultats:/Results NomDuWorkflow.sif addresse_IP_hôte port
```

Par exemple :

```bash
singularity run --app UI -B /home/jdupont/Documents/données:/Data -B /home/jdupont/Documents/résultats_workflow:/Results Mito_Assembler_Megahit.sif 127.0.0.1 1234
```

Il suffit ensuite de se connecter avec son navigateur à l'adresse et au port choisi dans la commande ('<http://127.0.0.1:1234'> dans l'exemple).

![Vue de l'interface web](interface_web.png "Interface web")

Dans l'interface on retrouve sur la gauche un paneau de commande avec des onglets pour chaque étape du workflow. Ces onglets permettent de configurer les étapes en choisissant les outils ainsi que leur paramètres. On trouve un onglet *Rule Graph* qui permet un fois les paramètre remplis de voir le schéma d'exécution du workflow. On trouve aussi le paramètre *Threads available* qui permet d'indiquer à **Snakemake** le nombre de coeurs maximum à mettre à sa disposition (dans le doute laissez la valeur par défaut). On peut ensuite choisir l'étape à atteindre dans le workflow. Si on veut que le workflow s'exécute en entier on choisit le paramètre *all*, sinon on choisit une étape de la liste. On trouve ensuite deux boutons pour exécuter ou arrếter le pipeline (ou workflow) ainsi que deux onglets permettant pour le premier de voir l'avancement du workflow et pour le second de visualiser le rapport final quand le workflow est terminé.

La première étape est de remplir l'onglet *Global parameters* en commançant par choisir un dossier de résultat. Vous pouvez choisir directement le dossier /Results ou y créer un sous dossier. Il est aussi possible de reprendre un dossier d'une analyse existante. Si un *Rule Graph* avait été obtenu ou si le workflow avait été lancé, les paramètres ont été sauvegardé et seront rechargés dans l'interface en selectionnant le dossier. Il faut ensuite choisir le dossier contenant les données, cela peut-être /Data ou un sous dossier.
Après avoir renseigné les paramètres pour les différentes étapes on peut aller sur l'onglet *Rule Graph* pour vérifier que le schéma du workflow correspond bien à l'analyse voulue. Il suffit ensuite de cliquer sur *Run pipeline* pour exécuter le workflow. On peut suivre l'execution du workflow dans la fenêtre *Running Workflow output* qui reprend la sortie standard de *Snakemake* et des outils lancés.

### 2.2 En ligne de commande

#### 2.2.1 En local

Pour lancer le workflow il faut le conteneur et un fichier de configuration. Pour obtenir ce fichier il faut soit se rendre dans le dépot Wapps, dans le dossier du workflow choisi puis dans sous-dossier files on trouve le fichier params.total.yml. Il suffit de faire une copie de ce fichier.

On peut aussi récupérer un fichier de paramètre qui a été généré lors de l'utilisation de l'applications **Shiny** (partie 2.1). Dans ce cas là si le workflow a été exécuté ou si le *rule graph* a été produit on retrouve alors le fichier *params.yml* dans le dossier de résultat spécifié dans l'application.

Une autre façon d'obtenir ce fichier est d'utiliser la commande getConfigfile du conteneur qui va copier un fichier de paramètre par défault dans le dossier courant. Pour cela il faut lancer la commande :

```bash
singularity run --app getConfigfile NomDuWorkflow.sif
```

Une fois ce fichier obtenu (params.yml) on peut y modifier les paramètres à sa guise.

Pour ce qui est des samples à analyser il faut renseigner plusieurs paramètres. Premièrement le dossier dans lequel les données sont présentes (le paramètre *sample_dir*). Il faut indiquer son chemin par rapport au repertoire lié à "/Data" dans la commande **Singularity**. Par exemple si les données sont dans le dossier "/home/jdupont/Documents/données/fastq" et que le dossier est lié de cette façon "-B /home/jdupont/Documents/données:/Data" (ce qui signifie /home/jdupont/Documents/données en local est équivalent à /Data dans le conteneur) il faut donc indiquer "sample_dir: /Data/fastq".

Ensuite il faut remplir les variables *SeOrPe*, *sample_suffix* et *samples*.

La variable SeOrPe sert à spécifier si dans l'analyse on utilise des reads single end ou paired end. Pour cela il faut juste spécifier "SeOrPe: SE" pour du single end et "SeOrPe: PE" pour du paired end.

Pour facilement remplir les paramètres *sample_suffix* et *samples* on peut utiliser le conteneur. Pour cela on lance l'application getSamples en liant le dossier de données et en passant en paramètre le dossier /Data qui vient d'être lié (ou un sous dossier si les données sont dans un sous dossier) et le paramètre *SeOrPe*. Par exemple :

```bash
singularity run --app getSamples -B /home/jdupont/Documents/données/:/Data NomDuWorkflow.sif /Data/fastq/ PE
```

On obtient un résultat du style :

```bash
{'samples': ['DRR016125', 'DRR016126', 'DRR016127'], 'suffix': '.fastq.gz'}
```

Dans ce cas on spécifie:

- sample_dir: "/Data/fastq/"
- SeOrPe: "PE"
- sample_suffix: ".fastq.gz"
- samples: ["DRR016125","DRR016126","DRR016127"]

Une fois ces paramètres spécifiés on peut enfin lancer le workflow de la manière suivante:

```bash
singularity run --app Snakemake -B /chemin/vers/les/données:/Data -B /dossier/de/résultats:/Results params.yml cores_number additional_snakemake_parameters
```

Une fois le workflow lancé les commandes s'enchainent jusqu'à la fin du workflow. On peut trouver les fichiers de logs dans le dossier logs du dossier de résultat.

#### 2.2.2 Sur un cluster

Travail en cours

## 3 Modifier un workflow

Il est possible de rapidement modifier les commandes lancées au cours du workflow. Pour cela il faut modifier le fichier Snakefile présent dans le dossier files du workflow (dans le dépot Wapps).

Ce fichier contient des règles permettant de définir l'exécution d'un outil. Prenons l'exemple de BWA mem, on trouve une règle pour les reads single end et une autre pour les reads paired end. Voici la règle dans le cas de reads paired end :

```python
rule bwa_mem_PE:
    input:
        **bwa_mem_inputs(),
        index = (
             config["bwa_index_path"]+"/index.amb",
             config["bwa_index_path"]+"/index.ann",
             config["bwa_index_path"]+"/index.bwt",
             config["bwa_index_path"]+"/index.pac",
             config["bwa_index_path"]+"/index.sa"
        )
    output:
        bam = config["results_dir"]+"/"+config["bwa_mem_PE_output_dir"]+"/{sample}.bam"
    log:
        config["results_dir"]+"/logs/bwa_mem/{sample}_bwa_mem_log.txt"
    threads:
        config["bwa_mem_threads"]
    params:
        indexPrefix =  config["bwa_index_path"]+"/index",
        quality0_multimapping = """| awk \'{{pos = index("XA:Z",\$0); if (pos>0) \$5=0;print}}\'""" if (config["bwa_mem_quality0_multimapping"] == True) else ""
    shell:
        "bwa mem "
        "-t {threads} "
        "{params.indexPrefix} "
        "{input.read} "
        "{input.read2} 2> {log} "
        "{params.quality0_multimapping} "
        "| samtools view -b 2>> {log} "
        "| samtools sort -@ {threads} > {output.bam} 2>> {log}"
```

La partie de la règle à modifier est la partie shell (ou run pour certaines règles). Dans cette partie on trouve la commande bash qui va être lancée, décorrée par les paramètres et les entrées et sorties gérées par **Snakemake**. Il suffit de rajouter une ligne avec le paramètre que l'on veut rajouter. Par exemple si on veut rajouter un read group au fichier d'alignement il suffit de rajouter le paramètre '-R' comme suit (Attention à ne pas oublier l'espace avant la fin de la ligne):

```python
    shell:
        "bwa mem "
        "-t {threads} "
        "-R '@RG\tID:foo\tSM:bar' "
        "{params.indexPrefix} "
        "{input.read} "
        "{input.read2} 2> {log} "
        "{params.quality0_multimapping} "
        "| samtools view -b 2>> {log} "
        "| samtools sort -@ {threads} > {output.bam} 2>> {log}"
```

Après les modifications souhaitées effectuées il faut reconstruire le conteneur comme indiqué plus haut pour pouvoir utiliser cette nouvelle version du workflow.

A noter que pour toute modification ou ajout qui pourrait être utile au workflow en général il est possible d'en faire la demande pour que ce soit implémenté directement sur le dépot. De même toute modification ou ajout plus complexe (par exemple ajout d'une étape au workflow) est possible sur demande.
