# WApps (Workflow Applications)

Ce dépot contient les fichiers générés à partir du dépot WAW ([Workflow Application Wrapper](https://gitlab.mbb.univ-montp2.fr/mmassaviol/waw)) pour les différents workflow existants :

- Metabarcoding (avec dada2)
- Mito_Assembler_Megahit (Megahit + MitoZ)
- MitoZ
- RADseq_ref (stacks)
- RADseq_denovo (stacks)
- RNAseq (pseudoalign (kallisto, Salmon) + expression différentielle (Edger, DESeq2))
- Virus_Assembler_Megahit (Megahit + Blast + Mapping)
- Variant_calling (bwa or bowtie and gatk or bcftools)
- Interop_report (Illumina InterOp)

Pour pouvoir construire un conteneur à partir de ce dépot et l'utiliser voir la doc suivante : [Construction et exécution](Documentation/How_to_build_and_run.md)
