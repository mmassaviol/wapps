import os
import re
import sys
import subprocess

def countRG(sample_dir, files_list):
    RG = list()
    for file in files_list:
        cmd = 'samtools view -H ' + os.path.join(sample_dir,file[0]) + ' | grep "^@RG"'
        res = subprocess.check_output(cmd, shell=True).decode("utf-8")
        RG = RG + res.splitlines()
    return len(set(RG))

def raw_bams(results_dir, sample_dir, STOP_CASES=dict()):
    samples = list()
    dicoSamples = dict() # sample_name: file(s)
    files = os.listdir(sample_dir)
    regex = re.compile(r"^(.+?).bam$")
    for file in files:
        res = re.match(regex, file)
        if res:
            if res.group(1) not in samples:
                samples.append(res.group(1))

            if res.group(1) not in dicoSamples.keys():
                dicoSamples[res.group(1)] = list()

            dicoSamples[res.group(1)].append(file)

    suffix = ".bam"
    with open(results_dir+"/samples.tsv","w") as sampleTab:
        sampleTab.write("sample\tbam_file")
        for sample in sorted(samples):
            sampleTab.write("\n"+sample+"\t"+"\t".join(sorted(dicoSamples[sample])))

    out = {'samples': sorted(samples), 'sample_suffix': suffix, 'dico': dicoSamples}
    out ["bam"] = os.path.join(sample_dir,"{sample}"+suffix)

    if "max_readGroups" in STOP_CASES.keys():
        if STOP_CASES["max_readGroups"] < countRG(sample_dir, dicoSamples.values()):
            exit("Pre-run checks: Bam file(s) must have only one read group (monosample workflow)")

    return out

#print(raw_bams(sys.argv[1],sys.argv[2]))
