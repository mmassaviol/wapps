FROM mbbteam/mbb_workflows_base:latest as alltools

RUN cd /opt/biotools \
 && wget https://github.com/samtools/samtools/releases/download/1.9/samtools-1.9.tar.bz2 \
 && tar -xvjf samtools-1.9.tar.bz2 \
 && cd samtools-1.9 \
 && ./configure && make \
 && cd .. \
 && mv samtools-1.9/samtools bin/samtools \
 && rm -r samtools-1.9 samtools-1.9.tar.bz2

RUN cd /opt/biotools/bin \
 && wget https://github.com/broadinstitute/picard/releases/download/2.20.8/picard.jar

RUN apt -y update && apt install -y openjdk-8-jre

RUN cd /opt/biotools \
 && wget -O GenomeAnalysisTK-3.6-0.tar.bz2 'https://storage.googleapis.com/gatk-software/package-archive/gatk/GenomeAnalysisTK-3.6-0-g89b7209.tar.bz2' \
 && mkdir gatk3 \
 && tar -C gatk3 -xjf GenomeAnalysisTK-3.6-0.tar.bz2 \
 && rm GenomeAnalysisTK-3.6-0.tar.bz2 \
 && rm -r gatk3/resources

RUN cd /opt/biotools \
 && wget https://github.com/broadinstitute/gatk/releases/download/4.1.1.0/gatk-4.1.1.0.zip \
 && unzip gatk-4.1.1.0.zip \
 && cd bin \
 && ln -s /opt/biotools/gatk-4.1.1.0/gatk gatk

RUN cd /opt/biotools \
 && wget https://github.com/samtools/bcftools/releases/download/1.9/bcftools-1.9.tar.bz2 \
 && tar -xvjf bcftools-1.9.tar.bz2 \
 && cd bcftools-1.9 \
 && ./configure --prefix=/opt/biotools \
 && make -j 10 \
 && make install \
 && mv bcftools /opt/biotools/bin/ \
 && cd .. && rm -r bcftools-1.9.tar.bz2 bcftools-1.9

RUN apt -y update && apt install -y tabix

RUN cd /opt/biotools \
 && wget https://github.com/google/deepvariant/archive/v0.9.0.tar.gz \
 && tar -xvzf v0.9.0.tar.gz \
 && mv deepvariant-0.9.0 /opt/deepvariant \
 && sed -i "/sudo -H apt-get -qq -y install python-dev python-pip python-wheel > \/dev\/null/d" /opt/deepvariant/run-prereq.sh \
 && sed -i "/python -m pip install .* --upgrade --force-reinstall pip/d" /opt/deepvariant/run-prereq.sh

RUN cd /opt/deepvariant \
 && apt-get install -y python-dev python-pip \
 && pip install virtualenv \
 && virtualenv -p /usr/bin/python2.7 deepvariant_env \
 && /bin/bash -c "source deepvariant_env/bin/activate && pip install pip==19.3.1"

RUN cd /opt/deepvariant \
 && /bin/bash -c "source deepvariant_env/bin/activate && PYTHONPATH=/opt/deepvariant/deepvariant_env/lib/python2.7 ./build-prereq.sh" \
 && /bin/bash -c "source deepvariant_env/bin/activate && PATH="${HOME}/bin:${PATH}" ./build_release_binaries.sh"

RUN cd /opt \
 && cp /opt/deepvariant/bazel-genfiles/licenses.zip . \
 && mkdir /opt/deepvariant/bin/ \
 && cd /opt/deepvariant/bin/ \
 && cp /opt/deepvariant/run-prereq.sh . \
 && cp /opt/deepvariant/settings.sh . \
 && cp /opt/deepvariant/bazel-bin/deepvariant/make_examples.zip . \
 && cp /opt/deepvariant/bazel-bin/deepvariant/call_variants.zip . \
 && cp /opt/deepvariant/bazel-bin/deepvariant/postprocess_variants.zip . \
 && cp /opt/deepvariant/bazel-bin/deepvariant/model_train.zip . \
 && cp /opt/deepvariant/bazel-bin/deepvariant/model_eval.zip . \
 && cp /opt/deepvariant/scripts/run_deepvariant.py . \
 && /bin/bash -c "source /opt/deepvariant/deepvariant_env/bin/activate && ./run-prereq.sh"

RUN BASH_HEADER='#!/bin/bash' \
 && printf "%s\n%s\n" "${BASH_HEADER}" 'python /opt/deepvariant/bin/make_examples.zip "$@"' > /opt/deepvariant/bin/make_examples \
 && printf "%s\n%s\n" "${BASH_HEADER}" 'python /opt/deepvariant/bin/call_variants.zip "$@"' > /opt/deepvariant/bin/call_variants \
 && printf "%s\n%s\n" "${BASH_HEADER}" 'python /opt/deepvariant/bin/postprocess_variants.zip "$@"' > /opt/deepvariant/bin/postprocess_variants \
 && printf "%s\n%s\n" "${BASH_HEADER}" 'python /opt/deepvariant/bin/model_train.zip "$@"' > /opt/deepvariant/bin/model_train \
 && printf "%s\n%s\n" "${BASH_HEADER}" 'python /opt/deepvariant/bin/model_eval.zip "$@"' > /opt/deepvariant/bin/model_eval \
 && printf "%s\n%s\n" "${BASH_HEADER}" 'python -u /opt/deepvariant/bin/run_deepvariant.py "$@"' > /opt/deepvariant/bin/run_deepvariant \
 && printf "%s\n%s\n" "${BASH_HEADER}" 'python -u /opt/deepvariant/bin/vcf_stats_report.py "$@"' > /opt/deepvariant/bin/vcf_stats_report \
 && chmod +x /opt/deepvariant/bin/make_examples /opt/deepvariant/bin/call_variants /opt/deepvariant/bin/postprocess_variants /opt/deepvariant/bin/model_train /opt/deepvariant/bin/model_eval /opt/deepvariant/bin/run_deepvariant \
 && mkdir /opt/bin \
 && cd /opt/bin \
 && wget https://github.com/dnanexus-rnd/GLnexus/releases/download/v1.2.1/glnexus_cli \
 && wget https://raw.githubusercontent.com/dnanexus-rnd/GLnexus/v1.2.1/LICENSE -O glnexus_cli.LICENSE \
 && chmod +rx /opt/bin/glnexus_cli \
 && mkdir /opt/models \
 && mkdir /opt/models/wgs \
 && cd /opt/models/wgs \
 && wget https://storage.googleapis.com/deepvariant/models/DeepVariant/0.9.0/DeepVariant-inception_v3-0.9.0+data-wgs_standard/model.ckpt.data-00000-of-00001 \
 && wget https://storage.googleapis.com/deepvariant/models/DeepVariant/0.9.0/DeepVariant-inception_v3-0.9.0+data-wgs_standard/model.ckpt.index \
 && wget https://storage.googleapis.com/deepvariant/models/DeepVariant/0.9.0/DeepVariant-inception_v3-0.9.0+data-wgs_standard/model.ckpt.meta \
 && chmod +r /opt/models/wgs/model.ckpt* \
 && mkdir /opt/models/wes \
 && cd /opt/models/wes \
 && wget https://storage.googleapis.com/deepvariant/models/DeepVariant/0.9.0/DeepVariant-inception_v3-0.9.0+data-wes_standard/model.ckpt.data-00000-of-00001 \
 && wget https://storage.googleapis.com/deepvariant/models/DeepVariant/0.9.0/DeepVariant-inception_v3-0.9.0+data-wes_standard/model.ckpt.index \
 && wget https://storage.googleapis.com/deepvariant/models/DeepVariant/0.9.0/DeepVariant-inception_v3-0.9.0+data-wes_standard/model.ckpt.meta \
 && chmod +r /opt/models/wes/model.ckpt* \
 && apt-get -y update \
 && apt-get install -y parallel \
 && /bin/bash -c "source /opt/deepvariant/deepvariant_env/bin/activate && python -m pip install pip==9.0.3 && pip install absl-py==0.7.1"

RUN cd /opt/biotools \
 && git clone https://gitlab.mbb.univ-montp2.fr/mmassaviol/mbb_mqc_plugin.git \
 && cd mbb_mqc_plugin \
 && python3 setup.py install

RUN cd /opt/biotools/bin \
 && wget https://github.com/ekg/freebayes/releases/download/v1.3.1/freebayes-v1.3.1 -O freebayes \
 && chmod +x freebayes

RUN apt-get install -y vcftools

RUN Rscript -e 'install.packages("UpSetR",Ncpus=8, clean=TRUE);library("UpSetR")'

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

#This part is necessary to run on ISEM cluster
RUN mkdir -p /share/apps/bin \
 && mkdir -p /share/apps/lib \
 && mkdir -p /share/apps/gridengine \
 && mkdir -p /share/bio \
 && mkdir -p /opt/gridengine \
 && mkdir -p /export/scrach \
 && mkdir -p /usr/lib64 \
 && ln -s /bin/bash /bin/mbb_bash \
 && ln -s /bin/bash /bin/isem_bash \
 && /usr/sbin/groupadd --system --gid 400 sge \
 && /usr/sbin/useradd --system --uid 400 --gid 400 -c GridEngine --shell /bin/true --home /opt/gridengine sge

EXPOSE 3838
CMD ["Rscript", "-e", "setwd('/sagApp/'); shiny::runApp('/sagApp/app.R',port=3838 , host='0.0.0.0')"]


FROM alltools

COPY files /workflow
COPY sagApp /sagApp

