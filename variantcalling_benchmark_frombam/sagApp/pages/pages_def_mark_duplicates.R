tabmark_duplicates = fluidPage(

box(title = "Parameters :", width = 12, status = "primary", collapsible = TRUE, solidHeader = TRUE,

	selectInput("selectmark_duplicates", label = "Select the tool to use : ", selected = "Picard_MarkDuplicates", choices = list("Picard_MarkDuplicates" = "Picard_MarkDuplicates", "null" = "null")),

conditionalPanel(condition = "input.selectmark_duplicates == 'Picard_MarkDuplicates'",box(title = "Picard Mark Duplicates", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		numericInput("Picard_MarkDuplicates_threads", label = "Number of threads to use", min = 1, max = NA, step = 1, width =  "auto", value = 4),

		checkboxInput("Picard_MarkDuplicates_remove_all_duplicates", label = "remove_all_duplicates : If true do not write duplicates to the output file instead of writing them with appropriate flags set.", value = TRUE),

		numericInput("Picard_MarkDuplicates_samtools_memory", label = "-m parameter for samtools sort: Memory allocated per thread for samtools_sort (in Gb).", min = , max = , step = , width =  "auto", value = 2),

		p("Picard Mark Duplicates: This tool locates and tags duplicate reads in a BAM or SAM file, where duplicate reads are defined as originating from a single fragment of DNA."),

		p("Website : ",a(href="https://broadinstitute.github.io/picard/","https://broadinstitute.github.io/picard/",target="_blank")),

		p("Documentation : ",a(href="https://broadinstitute.github.io/picard/command-line-overview.html","https://broadinstitute.github.io/picard/command-line-overview.html",target="_blank"))

	)),
conditionalPanel(condition = "input.selectmark_duplicates == 'null'",box(title = "null", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		p("null: Skip this step")

	))))


